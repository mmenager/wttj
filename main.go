package main

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/mmenager/wttj/pkg/geographical"
	"gitlab.com/mmenager/wttj/pkg/jobs"
	"gitlab.com/mmenager/wttj/pkg/utils"
	"gitlab.com/mmenager/wttj/web"
)

var allJobs []*jobs.JobOffer
var err error

const CONTINENTS_FILE = "data/continents-of-the-world.geojson"
const PROFESSIONS_FILE = "data/technical-test-professions.csv"
const JOBS_FILE = "data/technical-test-jobs.csv"

func main() {
	if len(os.Args) == 2 && os.Args[1] == "--help" {
		fmt.Print(`Usage:
- wttj (script mode)
- wttj serve (api mode)
`)
		return
	}

	// Reading the jobs
	j := utils.ReadCSVFile(JOBS_FILE)
	allJobs, err = jobs.GetJobOffers(j)
	if err != nil {
		fmt.Println("Error while retreiving jobs:", err)
	}

	// Script mode (question 1)
	if len(os.Args) == 1 {
		a, err := geographical.CountContinentsJobsOffers(allJobs, CONTINENTS_FILE, PROFESSIONS_FILE)
		if err != nil {
			fmt.Println("Error while counting jobs:", err)
		}
		jsonString, _ := json.MarshalIndent(a, "", "  ")
		fmt.Println(string(jsonString))
	}

	// API mode (question 2)
	if len(os.Args) > 1 && os.Args[1] == "serve" {
		web.Serve(allJobs)
	}
}
