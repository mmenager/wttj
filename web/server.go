package web

import (
	"net/http"
	"strconv"

	"gitlab.com/mmenager/wttj/pkg/jobs"

	"github.com/cozy/echo"
	"gitlab.com/mmenager/wttj/pkg/geographical"
)

var jobsOffers []*jobs.JobOffer

// Returns the jobs in the radius of the point
func nearJobs(c echo.Context) error {
	long, err := strconv.ParseFloat(c.QueryParam("long"), 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Long is missing or malformed")
	}
	lat, err := strconv.ParseFloat(c.QueryParam("lat"), 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "Lat is missing or malformed")
	}
	rad, err := strconv.ParseFloat(c.QueryParam("radius"), 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "radius is missing or malformed")
	}

	// Translating to meters instead of kilometers
	// The distance result is given in meters
	res := geographical.GetJobsNearPoint(jobsOffers, long, lat, rad*1000)
	return c.JSON(http.StatusOK, res)
}

func Serve(j []*jobs.JobOffer) error {
	// Get the jobs in memory
	var err error
	jobsOffers = j

	if err != nil {
		return err
	}

	// Start server
	e := echo.New()
	e.GET("/near", nearJobs)
	e.Logger.Fatal(e.Start(":1323"))

	return nil
}
