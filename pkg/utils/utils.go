package utils

import (
	"encoding/csv"
	"log"
	"os"
)

func ReadCSVFile(filename string) *csv.Reader {
	file, err := os.Open(filename) // For read access.
	if err != nil {
		log.Fatal(err)
	}

	csvReader := csv.NewReader(file)
	csvReader.TrimLeadingSpace = true

	return csvReader
}
