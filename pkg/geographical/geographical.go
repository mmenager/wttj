// See https://public.opendatasoft.com/explore/dataset/continents-of-the-world/information/
package geographical

import (
	"fmt"
	"io/ioutil"
	"strconv"

	"gitlab.com/mmenager/wttj/pkg/jobs"

	"github.com/paulmach/orb"
	"github.com/paulmach/orb/geo"
	"github.com/paulmach/orb/geojson"
	"github.com/paulmach/orb/planar"
)

// UNKNOWN const represents an unknown location for a job offer
const UNKNOWN = "Unknown"

type jobLocation struct {
	Continent      string
	ProfessionName string
}

type nearJobOffer struct {
	JobOffer *jobs.JobOffer `json:"job_offer"`
	Distance string         `json:"distance"`
}

func CountContinentsJobsOffers(jobsOffers []*jobs.JobOffer, continentsFile string, profsFile string) (map[string]map[string]int, error) {
	b, _ := ioutil.ReadFile(continentsFile)
	featureCollection, _ := geojson.UnmarshalFeatureCollection(b)

	locations := make(chan *jobLocation)

	// Find the location for each job
	for _, j := range jobsOffers {
		go FindJobOfferContinent(j, locations, featureCollection, profsFile)
	}

	// Computing the goroutines results
	final := map[string]map[string]int{
		"Total": map[string]int{},
	}
	for range jobsOffers {
		jobResult := <-locations

		// Initializations
		if _, ok := final[jobResult.Continent]; !ok {
			final[jobResult.Continent] = map[string]int{}
		}
		if _, ok := final[jobResult.Continent][jobResult.ProfessionName]; !ok {
			final[jobResult.Continent][jobResult.ProfessionName] = 0
		}
		if _, ok := final["Total"][jobResult.ProfessionName]; !ok {
			final["Total"][jobResult.ProfessionName] = 0
		}

		final[jobResult.Continent][jobResult.ProfessionName] += 1
		final["Total"][jobResult.ProfessionName] += 1

	}

	close(locations)

	return final, nil
}

// GetJobsNearPoint returns a job offer list near a point
func GetJobsNearPoint(jobsOffers []*jobs.JobOffer, long, lat, radius float64) []*nearJobOffer {
	nearJobs := []*nearJobOffer{}
	paramPoint := orb.Point{long, lat}

	for _, j := range jobsOffers {
		// Skip unknown job locations
		if j.OfficeLongitude == nil || j.OfficeLatitude == nil {
			continue
		}

		jobPoint := orb.Point{*j.OfficeLongitude, *j.OfficeLatitude}
		dist := geo.DistanceHaversine(jobPoint, paramPoint) // Returns the distance in meters

		if dist <= radius {
			d := HumanizeDistance(dist)
			nearJobs = append(nearJobs, &nearJobOffer{JobOffer: j, Distance: d})
		}
	}

	return nearJobs
}

// HumanizeDistance returns a raw distance in meters to a human-readable
// Ex: 12345.6789 => "12,3 km"
func HumanizeDistance(distanceMeters float64) string {
	if distanceMeters == 0 {
		return strconv.FormatFloat(distanceMeters, 'f', 0, 64) + " km"
	}
	distanceKilometers := distanceMeters / 1000
	return fmt.Sprintf("%.1f km", distanceKilometers)
}

// FindJobOfferContinent iterates over continent polygons to locate a job offer
func FindJobOfferContinent(j *jobs.JobOffer, locations chan *jobLocation, featureCollection *geojson.FeatureCollection, profsFile string) {
	// Retreiving the job category
	jobCat := jobs.GetJobProfCategoryName(j, profsFile)

	// We cannot locate the job offer if the long or lat is unknown
	if j.OfficeLatitude == nil || j.OfficeLongitude == nil {
		locations <- &jobLocation{
			Continent:      UNKNOWN,
			ProfessionName: jobCat,
		}
		return
	}

	// In the right case, we are going to iterate over continents to find the
	// good one
	point := orb.Point{*j.OfficeLongitude, *j.OfficeLatitude}

	// Iterate over features (= continents)
	for _, feature := range featureCollection.Features {
		continentName := feature.Properties["continent"].(string)
		multiPoly, _ := feature.Geometry.(orb.MultiPolygon)

		if planar.MultiPolygonContains(multiPoly, point) {
			// Continent was found
			locations <- &jobLocation{
				Continent:      continentName,
				ProfessionName: jobCat,
			}
			return
		}
	}
	// If we get here, it means that we did not found a matching continent
	locations <- &jobLocation{
		Continent:      UNKNOWN,
		ProfessionName: jobCat,
	}
	return
}
