package geographical

import (
	"testing"

	"gitlab.com/mmenager/wttj/pkg/jobs"
	"github.com/stretchr/testify/assert"
)

func TestGetJobsNearPoint(t *testing.T) {
	long1 := 7.749303
	lat1 := 48.581132

	long2 := 7.747563
	lat2 := 48.583536

	long3 := 13.303175
	lat3 := 52.470399

	offersList := []*jobs.JobOffer{
		// Strasbourg
		&jobs.JobOffer{
			Company:         "Foo",
			Name:            "Responsable d'achats",
			OfficeLongitude: &long1,
			OfficeLatitude:  &lat1,
		},
		// Strasbourg
		&jobs.JobOffer{
			Company:         "Foobar",
			Name:            "Compteur de nuages",
			OfficeLongitude: &long2,
			OfficeLatitude:  &lat2,
		},
		// Berlin
		&jobs.JobOffer{
			Company:         "Foobar Beer",
			Name:            "Testeur de bières",
			OfficeLongitude: &long3,
			OfficeLatitude:  &lat3,
		},
	}

	// Should return Strasbourg jobs
	nearOffers := GetJobsNearPoint(offersList, 7.765203, 48.586681, 10*1000) // 10km
	assert.Equal(t, 2, len(nearOffers))

	// Should return Strasbourg & Berlin jobs
	nearOffers = GetJobsNearPoint(offersList, 7.765203, 48.586681, 1000*1000) // 1000km
	assert.Equal(t, 3, len(nearOffers))

	// Should return no jobs
	nearOffers = GetJobsNearPoint(offersList, 7.765203, 48.586681, 10) // 10m
	assert.Equal(t, 0, len(nearOffers))
}

func TestCountContinentsJobsOffers(t *testing.T) {
	// Strasbourg
	long1 := 7.749303
	lat1 := 48.581132

	// Katmandou
	long2 := 85.244510
	lat2 := 27.611085

	// Buenos Aires
	long3 := -59.004914
	lat3 := -34.807443

	// Barcelona
	long4 := 2.178295
	lat4 := 41.393964

	uiID := 11
	backendID := 13
	stylistID := 35
	offersList := []*jobs.JobOffer{
		// Strasbourg
		&jobs.JobOffer{
			ProfessionID:    &stylistID,
			Company:         "Foo",
			Name:            "Directeur du défilé strasbourgeois",
			OfficeLongitude: &long1,
			OfficeLatitude:  &lat1,
		},
		&jobs.JobOffer{
			ProfessionID:    &backendID,
			Company:         "Foo2",
			Name:            "Développeur Brainfuck",
			OfficeLongitude: &long2,
			OfficeLatitude:  &lat2,
		},
		&jobs.JobOffer{
			ProfessionID:    &backendID,
			Company:         "Foo3",
			Name:            "Développeur Brainfuck",
			OfficeLongitude: &long3,
			OfficeLatitude:  &lat3,
		},
		&jobs.JobOffer{
			ProfessionID:    &uiID,
			Company:         "Foo3",
			Name:            "UI Designer Senior",
			OfficeLongitude: &long4,
			OfficeLatitude:  &lat4,
		},
		// Unknown location
		&jobs.JobOffer{
			ProfessionID:    &backendID,
			Company:         "Foo10",
			Name:            "azerty",
			OfficeLongitude: nil,
			OfficeLatitude:  nil,
		},
		&jobs.JobOffer{
			ProfessionID:    &backendID,
			Company:         "Foo10",
			Name:            "Pacific ocean",
			OfficeLongitude: nil,
			OfficeLatitude:  nil,
		},
	}

	profsFile := "../../data/technical-test-professions.csv"
	continentsFile := "../../data/continents-of-the-world.geojson"

	res, _ := CountContinentsJobsOffers(offersList, continentsFile, profsFile)
	assert.Equal(t, 1, res["Asia"]["Tech"])
	assert.Equal(t, 1, res["Europe"]["Créa"])
	assert.Equal(t, 1, res["Europe"]["Retail"])
	assert.Equal(t, 1, res["South America"]["Tech"])
	assert.Equal(t, 2, res["Unknown"]["Tech"])

	assert.Equal(t, 4, res["Total"]["Tech"])
	assert.Equal(t, 1, res["Total"]["Créa"])
	assert.Equal(t, 1, res["Total"]["Retail"])
}
