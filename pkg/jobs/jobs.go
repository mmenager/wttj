package jobs

import (
	"encoding/csv"
	"io"
	"log"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/mmenager/wttj/pkg/professions"
	"gitlab.com/mmenager/wttj/pkg/utils"
)

// UNKNOWN_CATEGORY represents an unknown category name for a job, probably
// because the job does not have a profession_id
const UNKNOWN_CATEGORY = "Unknown"

var professionsMap map[string]*professions.Profession

// JobOffer struct holds all the informations about a Job offer
type JobOffer struct {
	ProfessionID    *int     `json:"profession_id"`
	ContractType    string   `json:"contract_type"`
	Company         string   `json:"company"`
	Name            string   `json:"name"`
	OfficeLatitude  *float64 `json:"office_latitude"`
	OfficeLongitude *float64 `json:"office_longitude"`
}

// ParsedJobName is a struct holding a parsing result of a job name
type parsedJobName struct {
	companyName  string
	contractType string
	jobName      string
}

// GetJobOffers returns all the job offers from a reader
func GetJobOffers(jobs *csv.Reader) ([]*JobOffer, error) {
	offers := []*JobOffer{}

	jobs.Read() // Skipping the headers

	for {
		record, err := jobs.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		// Creates a job offer for each record
		job, err := NewJobOffer(record)
		if err != nil {
			return nil, err
		}

		offers = append(offers, job)
	}

	return offers, nil
}

// NewJobOffer parses a record and creates a Job Offer
func NewJobOffer(record []string) (*JobOffer, error) {
	// Handles empty profession_id
	var profID *int
	if record[0] != "" {
		tmp, err := strconv.Atoi(record[0])
		profID = &tmp
		if err != nil {
			return nil, err
		}
	}

	// Handles empty latitude
	var lat *float64
	if record[3] != "" {
		tmp, err := strconv.ParseFloat(record[3], 64)
		lat = &tmp
		if err != nil {
			return nil, err
		}
	}

	// Handles empty longitude
	var long *float64
	if record[4] != "" {
		tmp, err := strconv.ParseFloat(record[4], 64)
		long = &tmp
		if err != nil {
			return nil, err
		}
	}

	// If we have an empty contract type, try to fill it with a parsed one
	parsedJobName := parseJobName(record[2])
	contractType := record[1]
	if contractType == "" {
		contractType = parsedJobName.contractType
	}

	return &JobOffer{
		ProfessionID:    profID,
		ContractType:    contractType,
		Name:            parsedJobName.jobName,
		Company:         parsedJobName.companyName,
		OfficeLatitude:  lat,
		OfficeLongitude: long,
	}, nil
}

func initProfsMap(filename string) error {
	var err error
	p := utils.ReadCSVFile(filename)
	professionsMap, err = professions.GetProfessions(p)
	if err != nil {
		return err
	}
	return nil
}

// GetJobProfCategoryName returns the category name of a job
func GetJobProfCategoryName(job *JobOffer, professionsFile string) string {
	if len(professionsMap) == 0 {
		initProfsMap(professionsFile)
	}
	if job.ProfessionID == nil {
		return UNKNOWN_CATEGORY
	}

	return professionsMap[strconv.Itoa(*job.ProfessionID)].CategoryName
}

// parseJobName tries to find a company name and/or a job type in the job title
func parseJobName(jobName string) *parsedJobName {
	// Initializing regex
	jobNameRegex := regexp.MustCompile("\\[(.*?)\\]")

	// Creating a basic struct wich will be always returned
	parsedJobName := &parsedJobName{
		jobName: jobName,
	}

	matches := jobNameRegex.FindAllStringSubmatch(jobName, 2)
	if len(matches) > 0 {
		for _, match := range matches {
			// First, trim the contract type / company from the job name
			cleaned := strings.Replace(parsedJobName.jobName, match[0], "", -1)
			parsedJobName.jobName = strings.TrimSpace(cleaned)

			// Then, check if it is a company name
			matched := match[1]
			if IsContractType(matched) {
				parsedJobName.contractType = matched
			} else {
				parsedJobName.companyName = matched
			}
		}
	}

	return parsedJobName
}

// Check if a matched string is a contract type or a company name
func IsContractType(match string) bool {
	// Matching jobtypes
	contractTypes := []string{"stage", "cdd", "cdi", "alternance"}

	for _, ct := range contractTypes {
		trimmed := strings.TrimSpace(match)
		lowered := strings.ToLower(trimmed)
		if strings.Contains(lowered, ct) {
			return true
		}
	}

	return false
}
