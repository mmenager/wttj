package jobs

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEmptyProfessionID(t *testing.T) {
	// [ INTERNSHIP Candidature spontanée: STAGE 45.7360395 4.8360923]
	record := []string{"", "INTERNSHIP", "Candidature spontanée: STAGE", "45.7360395", "4.8360923"}
	job, err := NewJobOffer(record)

	assert.NoError(t, err)
	assert.Nil(t, job.ProfessionID)
	assert.Equal(t, "INTERNSHIP", job.ContractType)
	assert.Equal(t, "Candidature spontanée: STAGE", job.Name)
	assert.Equal(t, 45.7360395, *job.OfficeLatitude)
	assert.Equal(t, 4.8360923, *job.OfficeLongitude)
}

func TestEmptyLatitude(t *testing.T) {
	// [2 INTERNSHIP Stage - Business Developer - Lille  ]
	record := []string{"2", "INTERNSHIP", "Stage - Business Developer - Lille", "", ""}
	job, err := NewJobOffer(record)

	assert.NoError(t, err)
	assert.Equal(t, 2, *job.ProfessionID)
	assert.Equal(t, "INTERNSHIP", job.ContractType)
	assert.Equal(t, "Stage - Business Developer - Lille", job.Name)
	assert.Nil(t, job.OfficeLatitude)
	assert.Nil(t, job.OfficeLongitude)
}

func TestGoodJob(t *testing.T) {
	record := []string{"7", "TEMPORARY", "[Sephora SA] CDD Responsable Category Management eCommerce Europe", "48.8661039", "2.305791"}
	job, err := NewJobOffer(record)

	assert.NoError(t, err)
	assert.Equal(t, 7, *job.ProfessionID)
	assert.Equal(t, "TEMPORARY", job.ContractType)
	assert.Equal(t, "CDD Responsable Category Management eCommerce Europe", job.Name)
	assert.Equal(t, "Sephora SA", job.Company)
	assert.Equal(t, 48.8661039, *job.OfficeLatitude)
	assert.Equal(t, 2.305791, *job.OfficeLongitude)
}

func TestGoodJobWithoutCompany(t *testing.T) {
	record := []string{"7", "TEMPORARY", "CDD Responsable Category Management eCommerce Europe", "48.8661039", "2.305791"}
	job, err := NewJobOffer(record)

	assert.NoError(t, err)
	assert.Equal(t, 7, *job.ProfessionID)
	assert.Equal(t, "TEMPORARY", job.ContractType)
	assert.Equal(t, "CDD Responsable Category Management eCommerce Europe", job.Name)
	assert.Equal(t, 48.8661039, *job.OfficeLatitude)
	assert.Equal(t, 2.305791, *job.OfficeLongitude)
}

func TestGoodJobWithContractTypeInName(t *testing.T) {
	record := []string{"7", "", "[CDD] Responsable Category Management eCommerce Europe", "48.8661039", "2.305791"}
	job, err := NewJobOffer(record)

	assert.NoError(t, err)
	assert.Equal(t, 7, *job.ProfessionID)
	assert.Equal(t, "CDD", job.ContractType)
	assert.Equal(t, "Responsable Category Management eCommerce Europe", job.Name)
	assert.Equal(t, 48.8661039, *job.OfficeLatitude)
	assert.Equal(t, 2.305791, *job.OfficeLongitude)
}

func TestGoodJobWithContractTypeAndCompanyInName(t *testing.T) {
	record := []string{"7", "", "[Sephora SA] Responsable Category Management eCommerce Europe [CDI]", "48.8661039", "2.305791"}
	job, err := NewJobOffer(record)

	assert.NoError(t, err)
	assert.Equal(t, 7, *job.ProfessionID)
	assert.Equal(t, "CDI", job.ContractType)
	assert.Equal(t, "Responsable Category Management eCommerce Europe", job.Name)
	assert.Equal(t, "Sephora SA", job.Company)
	assert.Equal(t, 48.8661039, *job.OfficeLatitude)
	assert.Equal(t, 2.305791, *job.OfficeLongitude)
}

func TestGoodJobWithContractTypeAlreadyExisting(t *testing.T) {
	record := []string{"7", "TEMPORARY", "[Sephora SA] Responsable Category Management eCommerce Europe [CDI]", "48.8661039", "2.305791"}
	job, err := NewJobOffer(record)

	assert.NoError(t, err)
	assert.Equal(t, 7, *job.ProfessionID)
	assert.Equal(t, "TEMPORARY", job.ContractType)
	assert.Equal(t, "Responsable Category Management eCommerce Europe", job.Name)
	assert.Equal(t, "Sephora SA", job.Company)
	assert.Equal(t, 48.8661039, *job.OfficeLatitude)
	assert.Equal(t, 2.305791, *job.OfficeLongitude)
}

func TestGetCompanyName(t *testing.T) {
	jobName1 := "[Sephora SA] CDD Responsable Category Management eCommerce Europe"
	jobName2 := "[Louis Vuitton North America] Store Planning Seasonal"
	jobName3 := "[Acqua Di Parma] Junior Controller"
	jobName4 := "[CDD] Gestionnaire Administration du Personnel H/F"
	jobName5 := "UI Designer [Alternance]"
	jobName6 := "[Foobar Company] UI Designer [Alternance]"
	jobName7 := "UI Designer [Alternance / Stage]"

	r1 := parseJobName(jobName1)
	assert.Equal(t, "Sephora SA", r1.companyName)
	assert.Equal(t, "", r1.contractType)

	r2 := parseJobName(jobName2)
	assert.Equal(t, "Louis Vuitton North America", r2.companyName)
	assert.Equal(t, "", r2.contractType)

	r3 := parseJobName(jobName3)
	assert.Equal(t, "Acqua Di Parma", r3.companyName)
	assert.Equal(t, "", r3.contractType)

	r4 := parseJobName(jobName4)
	assert.Equal(t, "", r4.companyName)
	assert.Equal(t, "CDD", r4.contractType)

	r5 := parseJobName(jobName5)
	assert.Equal(t, "", r5.companyName)
	assert.Equal(t, "Alternance", r5.contractType)

	r6 := parseJobName(jobName6)
	assert.Equal(t, "Foobar Company", r6.companyName)
	assert.Equal(t, "Alternance", r6.contractType)

	r7 := parseJobName(jobName7)
	assert.Equal(t, "", r7.companyName)
	assert.Equal(t, "Alternance / Stage", r7.contractType)
}
