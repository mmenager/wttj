package professions

import (
	"encoding/csv"
	"io"
	"log"
)

// Profession holds the informations about a profession
type Profession struct {
	Name         string `json:"name"`
	CategoryName string `json:"category_name"`
}

// GetProfessions returns all the professions from a reader
func GetProfessions(professions *csv.Reader) (map[string]*Profession, error) {
	profs := map[string]*Profession{}

	professions.Read() // Skipping the headers

	for {
		record, err := professions.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		// Creates a prof for each record
		prof, err := NewProfession(record)
		if err != nil {
			return nil, err
		}

		profs[record[0]] = prof
	}

	return profs, nil
}

// NewProfession parses a record and creates a Profession
func NewProfession(record []string) (*Profession, error) {
	return &Profession{
		Name:         record[1],
		CategoryName: record[2],
	}, nil
}
