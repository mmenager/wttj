# WTTJ jobs offers

This repo contains the code for WTTJ backend technical test.

- `pkg/geographical` contains all the geographical-related functions, like
  computing the distance between two points or the continent of a job offer
- `pkg/jobs` contains all the job-related functions (file/job parsing,
  job-profession association, company name matching,...)
- `pkg/professions` contains the professions file parsing
- `web/server` has the API logic

## Executing / Building

Firstly, get the repo and its dependencies
```shell
$ go get gitlab.com/mmenager/wttj
$ cd $GOPATH/src/gitlab.com/mmenager/wttj
$ go get ./...
```

### Execution

    Note : an experimental "db mode" is available on the "spatialite" git branch
    $ go run main.go db

#### "Script" mode
``` $ go run main.go ```

#### "API" mode
``` $ go run main.go serve```

> The endpoint is available at:
``` http://localhost:1323/near```

**Mandatory query parameters**:
- **long**: point longitude
- **lat**: point latitude
- **radius**: distance to search (in km)

Example:
```http://localhost:1323/near?long=-118.4015664&lat=34.0675179&radius=20```


### Building

``` $ go build```

It compiles on Golang 1.12


## Testing


```shell
$ go get github.com/stretchr/testify/assert
$ go test ./...
```

## Troubleshooting

# macOSX

`2019/06/17 17:31:23 open data/technical-test-professions.csv: too many open files`

I do not develop on macOSX, but it seems that the high number of goroutines
breaks the binary on some laptops. Temporarily increase the ulimit if it
happens: `ulimit -n 5000`
